#!/usr/bin/env node
import * as exec from 'child_process';
import inquirer from 'inquirer';
import * as path from 'path';
import * as url from 'url';

async function welcome() {
  const __filename = url.fileURLToPath(import.meta.url);
  const __dirname = path.dirname(__filename);
  inquirer
    .prompt([
      {
        type: 'list',
        message: 'Select an option',
        name: 'options',
        choices: [
          'Create environment files',
          'Create database settings files',
          'Create linters files',
          'Create REST app',
          'Create REST resource',
          'Create GRPC app',
          'Create GRPC resource',
          'Create GRAPHQL app',
          'Create GRAPHQL resource',
          'Get validators',
          'Get Guards',
          'Get Decorators',
        ],
      },
    ])
    .then(({ options }) => {
      switch (options) {
        case 'Create environment files':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-enviroment-files'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          break;
        case 'Create database settings files':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-database-settings-files'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          break;
        case 'Create linters files':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-linters'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          break;
        case 'Create REST app':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-linters'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-enviroment-files'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-database-settings-files'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-rest-api'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          break;
        case 'Create GRPC app':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-linters'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-enviroment-files'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-database-settings-files'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-grpc-app'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-nest-cli'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
            shell: true,
          });
          break;
        case 'Create GRPC resource':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-grpc-resource'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
          });
          break;
        case 'Create REST resource':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-rest-resource'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
          });
          break;
        case 'Create GRAPHQL app':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-linters'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit',
              'inherit',
            ],
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-enviroment-files'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit',
              'inherit',
            ],
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-database-settings-files'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit',
              'inherit',
            ],
          });
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-graphql-app'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit',
              'inherit',
            ],
          });
          break;
        case 'Create GRAPHQL resource':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-graphql-resource'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
          });
          break;
        case 'Get Guards':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-guards'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
          });
          break;
        case 'Get validators':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-validators'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit', // inherit STDOUT from the parent process
              'inherit', // inherit STDERR from the parent process
            ],
          });
          break;
        case 'Get Decorators':
          exec.spawn('node', [path.join(__dirname) + '/tools/generate-decorators'], {
            detached: false,
            stdio: [
              'inherit', // inherit STDIN  from the parent process
              'inherit',
              'inherit',
            ],
          });
          break;
        default:
          console.log('Invalid option');
          break;
      }
    });
}
await welcome();
