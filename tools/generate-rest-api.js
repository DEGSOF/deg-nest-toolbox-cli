import * as gtf from 'generate-template-files';
import * as path from 'path';
import * as url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const cwd = process.env.INIT_CWD;
const filePath = path.join(cwd, 'src', 'app.module.ts');
const outputPath = path.join(cwd, 'src');

await gtf.generateTemplateFilesBatch([
  {
    entry: {
      folderPath: path.join(__dirname, 'templates', 'rest-app', 'app.module.ts'),
    },
    option: '',
    defaultCase: gtf.CaseConverterEnum.KebabCase,
    dynamicReplacers: [
      {
        slot: '__entity__',
        slotValue: path.join('templates', 'rest-app', 'app.module.ts'),
      },
    ],
    output: {
      path: path.join(outputPath, 'app.module.ts'),
      overwrite: true,
    },
    onComplete: (result) => {
      console.log('App module generated');
    },
  },
]);

await gtf.generateTemplateFilesBatch([
  {
    entry: {
      folderPath: path.join(__dirname, 'templates', 'rest-app', 'modules'),
    },
    option: '',
    defaultCase: gtf.CaseConverterEnum.KebabCase,
    dynamicReplacers: [
      {
        slot: '__entity__',
        slotValue: 'modules',
      },
    ],
    output: {
      path: path.join(outputPath, 'modules'),
      overwrite: true,
    },
    onComplete: (result) => {
      console.log('Modules folder generated');
    },
  },
]);

await gtf.generateTemplateFilesBatch([
  {
    option: '',
    entry: {
      folderPath: path.join(__dirname, 'templates', 'rest-app', 'main.ts'),
    },
    defaultCase: gtf.CaseConverterEnum.KebabCase,
    dynamicReplacers: [
      {
        slot: '__entity__',
        slotValue: 'main',
      },
    ],
    output: {
      path: path.join(outputPath, 'main.ts'),
      overwrite: true,
    },
    onComplete: (result) => {
      console.log('Main file generated');
    },
  },
]);
