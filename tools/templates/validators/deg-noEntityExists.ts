import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { DataSource } from 'typeorm';

@ValidatorConstraint({ name: 'noExistsEntityRule', async: true })
@Injectable()
export class NoExistsEntityRule implements ValidatorConstraintInterface {
  constructor(@InjectDataSource() private readonly connection: DataSource) {}

  async validate(value: number | string, args: ValidationArguments): Promise<boolean> {
    const [entity, column] = args.constraints;
    if (value?.toString().length == 0) return false;
    const repository = this.connection.getRepository(entity);
    try {
      const record = await repository.findOne({ where: { [column]: value } });
      return record === null;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}

export function noExistsEntity(entity, column: string, validationOptions?: ValidationOptions): (object: any, propertyName: string) => void {
  return function (object: any, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [entity, column],
      validator: NoExistsEntityRule,
    });
  };
}
