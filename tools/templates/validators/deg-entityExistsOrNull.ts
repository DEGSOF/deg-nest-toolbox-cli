import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { DataSource } from 'typeorm';

@ValidatorConstraint({ name: 'EntityExistOrNullRule', async: true })
@Injectable()
export class EntityExistOrNullRule implements ValidatorConstraintInterface {
  constructor(@InjectDataSource() private readonly connection: DataSource) {}

  async validate(value: number | string, args: ValidationArguments): Promise<boolean> {
    const [entity, column] = args.constraints;
    const repository = this.connection.getRepository(entity);
    if (value?.toString().length > 0) {
      try {
        const record = await repository.findOne({ where: { [column]: value } });
        return record !== null;
      } catch (error) {
        throw new InternalServerErrorException(error);
      }
    }
    if (value != undefined && value.toString() == '') {
      return false;
    }
    return true;
  }
}

export function entityExistsOrNull(entity: object, column: string, validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string): void {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [entity, column],
      validator: EntityExistOrNullRule,
    });
  };
}
