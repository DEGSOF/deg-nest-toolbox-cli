import { GenericService } from '@dany-deg/toolbox-dev';
import { Inject, Injectable } from '@nestjs/common';

import { __entity__PascalCase__ } from './entities/__entity__.entity';
import { __entity__PascalCase__Repository } from './repository/__entity__.repository';

@Injectable()
export class __entity__PascalCase__Service extends GenericService<__entity__PascalCase__> {
  constructor(
    @Inject(__entity__PascalCase__Repository)
    private readonly repository: __entity__PascalCase__Repository,
  ) {
    super(repository);
  }
}
