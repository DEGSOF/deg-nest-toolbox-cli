import { TimedEntityWithLexicId } from '@dany-deg/toolbox-dev';
import { Entity } from 'typeorm';

@Entity('__entity__')
export class __entity__PascalCase__ extends TimedEntityWithLexicId {}
