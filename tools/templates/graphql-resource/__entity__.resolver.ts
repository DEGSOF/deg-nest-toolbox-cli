import { Args, Mutation, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { plainToInstance } from 'class-transformer';
import { RequestContext } from 'src/types/requestContext.type';

import { Create__entity__PascalCase__Dto } from './dto/create-__entity__.dto';
import { Update__entity__PascalCase__Dto } from './dto/update-__entity__.dto';
import { __entity__PascalCase__ } from './entities/__entity__.entity';

@Resolver('__entity__Query')
export class CompaniesResolver {
  constructor(private readonly _service: __entity__PascalCase__Service) {}

  @Query(() => Promise<{ find: FindCursorResponse }>, { name: '__entity__' })
  __entity__(): object {
    const companies = {};
    return companies;
  }

  @ResolveField('find')
  async find(@Args() findSettings: FindRequest): Promise<FindCursorResponse> {
    return await this._service.getList(findSettings);
  }

  @Mutation(() => __entity__PascalCase__, { name: 'create__entity__' }) //root query property name
  async create__entity__(@Args('__entity__') __entity__Args: Create__entity__PascalCase__Dto): Promise<__entity__PascalCase__> {
    try {
      console.log('__entity__', __entity__Args);
      const __entity__Object = plainToInstance(__entity__PascalCase__, __entity__Args, { enableCircularCheck: true });
      // bussines logic
      return await this._service.createAndReturn(__entity__Object);
    } catch (error) {
      throw error;
    }
  }

  @Mutation(() => __entity__PascalCase__, { name: 'update__entity__' }) //root query property name
  async updateCompany(@Args('__entity__') __entity__Args: Update__entity__PascalCase__Dto, @CURRENTUSER() currentUser: RequestContext): Promise<__entity__PascalCase__> {
    try {
      console.log('__entity__', __entity__Args);
      const __entity__Object = plainToInstance(__entity__PascalCase__, __entity__Args, { enableCircularCheck: true });
      // bussines logic
      return await this._service.createAndReturn(__entity__Object);
    } catch (error) {
      throw error;
    }
  }
}
