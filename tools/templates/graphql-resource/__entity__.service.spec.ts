import { createMock } from '@golevelup/ts-jest';
import { Test, TestingModule } from '@nestjs/testing';

import { __entity__PascalCase__Service } from './__entity__.service';

describe('__entity__PascalCase__service', () => {
    let service: __entity__PascalCase__Service;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [__entity__PascalCase__Service]
        })
            .useMocker(() => createMock())
            .compile();

        service = module.get<__entity__PascalCase__Service>(__entity__PascalCase__Service);
    });
    
});
