import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv';
import { join } from 'path';
import { DataSource } from 'typeorm';

config();
const configService = new ConfigService();
const password = configService.get('TYPEORM_PASSWORD');
export default new DataSource({
  type: 'mysql',
  host: configService.get('TYPEORM_HOST'),
  port: configService.get('TYPEORM_PORT'),
  username: configService.get('TYPEORM_USERNAME'),
  password: password,
  database: configService.get('TYPEORM_DATABASE'),
  logging: configService.get('NEED_SQL_LOG'),
  entityPrefix: configService.get('TYPEORM_ENTITY_PREFIX'),
  entities: [join(__dirname, '..', 'modules', '**', '*.entity{.ts,.js}')],
  migrations: [configService.get('TYPEORM_MIGRATIONS_DIR')],
  migrationsTableName: configService.get('TYPEORM_MIGRATIONS_TABLE_NAME'),
});
