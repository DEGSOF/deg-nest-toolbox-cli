import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { DataSource, DataSourceOptions } from 'typeorm';

export const databaseProviders = [
  TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    inject: [ConfigService],
    async useFactory(config: ConfigService) {
      return {
        type: 'mysql' as const,
        host: config.get('TYPEORM_HOST'),
        port: config.get('TYPEORM_PORT'),
        username: config.get('TYPEORM_USERNAME'),
        password: config.get('TYPEORM_PASSWORD'),
        database: config.get('TYPEORM_DATABASE'),
        entities: [join(__dirname, '..', 'modules', '**', '*.entity{.ts,.js}')],
        logging: config.get('NEED_SQL_LOG'),
        tablePrefix: config.get('TYPEORM_ENTITY_PREFIX'),
        entityPrefix: config.get('TYPEORM_ENTITY_PREFIX'),
        migrationsTableName: config.get('TYPEORM_MIGRATIONS_TABLE_NAME'),
        migrations: [__dirname + config.get('TYPEORM_MIGRATIONS_DIR')],
      } as DataSourceOptions;
    },
    dataSourceFactory: async (options) => {
      const dataSource = await new DataSource(options).initialize();
      return dataSource;
    },
  }),
];
