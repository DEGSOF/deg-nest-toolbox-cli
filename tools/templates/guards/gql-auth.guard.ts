import { ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';

/**
 * @description: This class is used to guard the routes that need authentication with JWT on GraphQL endpoints of the application
 * @class: HttpExceptionFilter
 * @extends AuthGuard
 * @property reflector: Reflector - used to get the metadata of the route
 * @method canActivate: boolean - method called to check if the route can be activated
 * @method getRequest: any - method called to get the request
 * @method handleRequest: any - method called to handle the request
 * @param: {ExecutionContext} context
 * @returns: {Observable<any>}
 */
@Injectable()
export class GqlAuthGuard extends AuthGuard('jwt') {
  public constructor(private readonly reflector: Reflector) {
    super();
  }

  getRequest(context: ExecutionContext): any {
    const ctx = GqlExecutionContext.create(context);
    const req = ctx.getContext().req;
    return req;
  }

  canActivate(context: ExecutionContext): any {
    const isPublic = this.reflector.get<boolean>('isPublic', context.getHandler());
    if (isPublic) {
      return true;
    }
    /* istanbul ignore next */
    return super.canActivate(context);
  }

  handleRequest(err, user): UnauthorizedException | any {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
