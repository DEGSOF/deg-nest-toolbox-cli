// Generated by GitHub Copilot
import { ExecutionContext } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { JwtAuthGRPCGuard } from './jwt-auth-grpc.guard';

describe('JwtAuthGRPCGuard', () => {
  let guard: JwtAuthGRPCGuard;
  let jwtServiceMock: JwtService;
  let executionContextMock: any;

  beforeEach(() => {
    jwtServiceMock = {
      decode: jest.fn().mockReturnThis(),
    } as any;
    executionContextMock = {
      switchToHttp: jest.fn(),
      switchToRpc: jest.fn().mockImplementation(() => ({
        getContext: jest.fn().mockImplementation(() => ({
          set: jest.fn(),
        })),
      })),
      switchToWs: jest.fn(),
      getClass: jest.fn(),
      getHandler: jest.fn(),
      getArgs: jest.fn(),
      getArgByIndex: jest.fn().mockReturnValue([
        {
          get: jest.fn().mockReturnValue({
            Authorization: ['Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'],
          }),
        },
        {
          get: jest.fn().mockReturnValue({
            Authorization: ['Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'],
          }),
        },
      ]),
      getType: jest.fn().mockReturnValue({ type: 'rpc' }),
      getArg: jest.fn(),
      getContext: jest.fn(),
      getMethod: jest.fn(),
      getMetadata: jest.fn(),
      getRpcContext: jest.fn(),
      getRpcArguments: jest.fn(),
      getRpcParams: jest.fn(),
    } as unknown as ExecutionContext;
    guard = new JwtAuthGRPCGuard(jwtServiceMock);
  });

  describe('canActivate', () => {
    it('should return false when no Authorization header is provided', async () => {
      executionContextMock.getType.mockReturnValue('rpc');
      executionContextMock.getArgByIndex.mockReturnValueOnce(undefined);
      const result = await guard.canActivate(executionContextMock);
      expect(result).toBe(false);
    });

    // Returns true if the request has a valid JWT token in the 'Authorization' header with the 'Bearer' prefix
    it('should return true when request has a valid JWT token', () => {
      // Arrange
      const context = {
        getType: jest.fn().mockReturnValue('rpc'),
        getArgByIndex: jest.fn().mockReturnValue({
          get: jest.fn().mockReturnValue(['Bearer validToken']),
        }),
        switchToHttp: jest.fn(),
        switchToRpc: jest.fn().mockImplementation(() => ({
          getContext: jest.fn().mockImplementation(() => ({
            set: jest.fn(),
          })),
        })),
        switchToWs: jest.fn(),
        getClass: jest.fn(),
        getHandler: jest.fn(),
        getArgs: jest.fn(),
        getArg: jest.fn(),
        getContext: jest.fn(),
        getMethod: jest.fn(),
        getMetadata: jest.fn(),
        getRpcContext: jest.fn(),
        getRpcArguments: jest.fn(),
        getRpcParams: jest.fn(),
      };
      const jwtServ = {
        decode: jest.fn().mockReturnValue({}),
      } as any;
      const guard = new JwtAuthGRPCGuard(jwtServ);

      // Act
      const result = guard.canActivate(context as ExecutionContext);

      // Assert
      expect(result).resolves.toStrictEqual({});
      expect(context.getArgByIndex).toHaveBeenCalledWith(1);
      expect(jwtServ.decode).toHaveBeenCalledWith('validToken');
    });

    it('should return false when request has a invalid JWT token', () => {
      // Arrange
      const context = {
        getType: jest.fn().mockReturnValue('rpc'),
        getArgByIndex: jest.fn().mockReturnValue({
          get: jest.fn().mockReturnValue(['InvalidToken']),
        }),
        switchToHttp: jest.fn(),
        switchToRpc: jest.fn().mockImplementation(() => ({
          getContext: jest.fn().mockImplementation(() => ({
            set: jest.fn(),
          })),
        })),
        switchToWs: jest.fn(),
        getClass: jest.fn(),
        getHandler: jest.fn(),
        getArgs: jest.fn(),
        getArg: jest.fn(),
        getContext: jest.fn(),
        getMethod: jest.fn(),
        getMetadata: jest.fn(),
        getRpcContext: jest.fn(),
        getRpcArguments: jest.fn(),
        getRpcParams: jest.fn(),
      };
      const jwtServ = {
        decode: jest.fn().mockReturnValue({}),
      } as any;
      const guard = new JwtAuthGRPCGuard(jwtServ);

      // Act
      const result = guard.canActivate(context as ExecutionContext);

      // Assert
      expect(result).resolves.toStrictEqual(false);
      expect(context.getArgByIndex).toHaveBeenCalledWith(1);
    });

    it('should return false when request has no header', () => {
      // Arrange
      const context = {
        getType: jest.fn().mockReturnValue('xxx'),
        getArgByIndex: jest.fn().mockReturnValue({
          get: jest.fn().mockReturnValue(['InvalidToken']),
        }),
        switchToHttp: jest.fn(),
        switchToRpc: jest.fn().mockImplementation(() => ({
          getContext: jest.fn().mockImplementation(() => ({
            set: jest.fn(),
          })),
        })),
        switchToWs: jest.fn(),
        getClass: jest.fn(),
        getHandler: jest.fn(),
        getArgs: jest.fn(),
        getArg: jest.fn(),
        getContext: jest.fn(),
        getMethod: jest.fn(),
        getMetadata: jest.fn(),
        getRpcContext: jest.fn(),
        getRpcArguments: jest.fn(),
        getRpcParams: jest.fn(),
      };
      const jwtServ = {
        decode: jest.fn().mockReturnValue({}),
      } as any;
      const guard = new JwtAuthGRPCGuard(jwtServ);

      // Act
      const result = guard.canActivate(context as ExecutionContext);

      // Assert
      expect(result).resolves.toStrictEqual(false);
    });
  });
});
