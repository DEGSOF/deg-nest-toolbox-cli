import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const CURRENTUSER = createParamDecorator((data: unknown, context: ExecutionContext) => {
  const ctx = GqlExecutionContext.create(context);
  const req = ctx.getContext().req;
  const requestContext = req.user;
  if (!requestContext) {
    throw Error('Authentication context not found');
  }
  return requestContext;
});
