import { createParamDecorator, ExecutionContext, ForbiddenException } from '@nestjs/common';

export const authUser = createParamDecorator((data: unknown, ctx: ExecutionContext): Partial<any> => {
  try {
    const request = ctx.switchToHttp().getRequest();
    return request;
  } catch (error) {
    throw new ForbiddenException();
  }
});
