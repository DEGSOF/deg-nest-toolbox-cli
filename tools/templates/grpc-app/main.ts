import 'reflect-metadata';

import { GRPCExceptionFilter } from '@dany-deg/toolbox-dev';
import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { useContainer } from 'class-validator';
import { join } from 'path';

import { AppModule } from './app.module';

/* eslint-disable quotes */
// generar api con el modelo de datos
async function bootstrap(): Promise<void> {
  /*****CONFIGURACION GRPC*****/
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.GRPC,
    options: {
      url: `0.0.0.0:${process.env.PORT}`,
      package: 'find_settings',
      loader: { keepCase: true },
      protoPath: [join('./src/protos/find-settings.proto')],
    },
  });
  useContainer(app.select(AppModule), {
    fallbackOnErrors: true,
  });
  app.useGlobalFilters(new GRPCExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors) => new BadRequestException(errors),
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );
  await app.listen();
  console.log(`Server running on port ${1}`);
}
bootstrap();
//npm run typeorm:generate-migration -name=init
//protoc --plugin=protoc-gen-ts_proto=.\node_modules\.bin\protoc-gen-ts_proto.cmd --ts_proto_out=src/core/interfaces/protos -I=src/protos src/protos/*.proto --ts_proto_opt=addGrpcMetadata=true
