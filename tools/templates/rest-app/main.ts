import 'reflect-metadata';

import { HttpExceptionFilter, ResponseInterceptor } from '@dany-deg/toolbox-dev';
import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import helmet from 'helmet';

import { AppModule } from './app.module';

/* eslint-disable quotes */
// generar api con el modelo de datos
async function bootstrap(): Promise<void> {
  /*****CONFIGURACION REST*****/
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api'); //prefijo
  app.use(
    helmet.contentSecurityPolicy({
      directives: {
        ...helmet.contentSecurityPolicy.getDefaultDirectives(),
        'img-src': ["'self'", "'cdn.jsdelivr.net'"],
        'script-src': ["'self'", 'cdn.jsdelivr.net', "'unsafe-inline'"],
      },
    }),
  );
  useContainer(app.select(AppModule), {
    fallbackOnErrors: true,
  });
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalInterceptors(new ResponseInterceptor());
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors) => new BadRequestException(errors),
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );
  const config = new DocumentBuilder()
    .addApiKey(
      {
        type: 'apiKey',
        in: 'header',
        name: 'x-api-key',
      },
      'x-api-key',
    )
    .addSecurityRequirements('x-api-key')
    .setTitle('NodeBackend Generic')
    .setDescription('The API description')
    .setVersion('0.0.1')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  app.enableCors();
  const configService = app.get(ConfigService);
  const port = configService.get('PORT');
  await app.listen(port);
  console.log(`Server running on port ${port}`);
}
bootstrap();
//npm run typeorm:generate-migration -name=init
//protoc --plugin=protoc-gen-ts_proto=.\node_modules\.bin\protoc-gen-ts_proto.cmd --ts_proto_out=src/core/interfaces/protos -I=src/protos src/protos/*.proto --ts_proto_opt=addGrpcMetadata=true
