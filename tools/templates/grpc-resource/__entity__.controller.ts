import { Controller } from '@nestjs/common';

import { __entity__PascalCase__Service } from './__entity__.service';

@Controller("__entity__")
export class __entity__PascalCase__Controller {
    constructor(private readonly _service: __entity__PascalCase__Service) {
    }
}
