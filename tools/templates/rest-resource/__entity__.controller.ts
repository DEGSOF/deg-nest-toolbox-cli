import { controllerFactory } from '@dany-deg/toolbox-dev';
import { Controller } from '@nestjs/common';

import { __entity__PascalCase__Service } from './__entity__.service';
import { Create__entity__PascalCase__Dto } from './dto/create-__entity__.dto';
import { Update__entity__PascalCase__Dto } from './dto/update-__entity__.dto';
import { __entity__PascalCase__ } from './entities/__entity__.entity';

@Controller('__entity__')
export class __entity__PascalCase__Controller extends controllerFactory<__entity__PascalCase__, Create__entity__PascalCase__Dto, Update__entity__PascalCase__Dto>(
  __entity__PascalCase__,
  Create__entity__PascalCase__Dto,
  Update__entity__PascalCase__Dto,
) {
  constructor(private readonly _service: __entity__PascalCase__Service) {
    super(_service);
  }
}
