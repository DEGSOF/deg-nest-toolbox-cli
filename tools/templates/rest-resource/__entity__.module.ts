import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { __entity__PascalCase__Controller } from './__entity__.controller';
import { __entity__PascalCase__Service } from './__entity__.service';
import { __entity__PascalCase__ } from './entities/__entity__.entity';
import { __entity__PascalCase__Repository } from './repository/__entity__.repository';

@Module({
  controllers: [__entity__PascalCase__Controller],
  providers: [__entity__PascalCase__Service, __entity__PascalCase__Repository],
  imports: [TypeOrmModule.forFeature([__entity__PascalCase__])],
  exports: [__entity__PascalCase__Service, __entity__PascalCase__Repository, TypeOrmModule],
})
export class __entity__PascalCase__Module {}
