import { createMock } from '@golevelup/ts-jest';
import { Test, TestingModule } from '@nestjs/testing';

import { __entity__PascalCase__Controller } from './__entity__.controller';
import { __entity__PascalCase__Service } from './__entity__.service';

describe('__entity__PascalCase__Controller', () => {
  let controller: __entity__PascalCase__Controller;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [__entity__PascalCase__Controller],
      providers: [__entity__PascalCase__Service],
    })
      .useMocker(() => createMock())
      .compile();

    controller = module.get<__entity__PascalCase__Controller>(__entity__PascalCase__Controller);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
