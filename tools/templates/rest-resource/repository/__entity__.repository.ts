import { GenericRepository } from '@dany-deg/toolbox-dev';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { __entity__PascalCase__ } from '../entities/__entity__.entity';

@Injectable()
export class __entity__PascalCase__Repository extends GenericRepository<__entity__PascalCase__> {
  constructor(
    @InjectRepository(__entity__PascalCase__)
    private repo: GenericRepository<__entity__PascalCase__>,
  ) {
    super(repo.target, repo.manager, repo.queryRunner);
  }
}
