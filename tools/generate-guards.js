import * as gtf from 'generate-template-files';
import * as path from 'path';
import * as url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const outputPath = process.env.INIT_CWD;

const generateFile = async (folderPath, slotValue, outputFileName) => {
  await gtf.generateTemplateFilesBatch([
    {
      option: '',
      entry: {
        folderPath: path.join(__dirname, folderPath),
      },
      defaultCase: gtf.CaseConverterEnum.KebabCase,
      dynamicReplacers: [
        {
          slot: '__entity__',
          slotValue,
        },
      ],
      output: {
        path: path.join(outputPath, outputFileName),
        overwrite: true,
      },
      onComplete: (result) => {
        console.log(`${outputFileName} file generated`);
      },
    },
  ]);
};

// use the generateFile function to generate the files from the templates/guards folder
await generateFile(path.sep + 'templates' + path.sep + 'guards' + path.sep + 'gql-auth.guard.ts', 'gql-auth.guard.ts', path.sep + 'src' + path.sep + 'guards' + path.sep + 'gql-auth.guard.ts');
await generateFile(
  path.sep + 'templates' + path.sep + 'guards' + path.sep + 'gql-auth.guard.spec.ts',
  'gql-auth.guard.spec.ts',
  path.sep + 'src' + path.sep + 'guards' + path.sep + 'gql-auth.guard.spec.ts',
);
await generateFile(
  path.sep + 'templates' + path.sep + 'guards' + path.sep + 'jwt-auth-grpc.guard.ts',
  'jwt-auth-grpc.guard',
  path.sep + 'src' + path.sep + 'guards' + path.sep + 'jwt-auth-grpc.guard',
);
await generateFile(
  path.sep + 'templates' + path.sep + 'guards' + path.sep + 'jwt-auth-grpc.guard.spec.ts',
  'jwt-auth-grpc.guard.spec.ts',
  path.sep + 'src' + path.sep + 'guards' + path.sep + 'jwt-auth-grpc.guard.spec.ts',
);
await generateFile(path.sep + 'templates' + path.sep + 'guards' + path.sep + 'jwt-auth.guard.ts', 'jwt-auth.guard.ts', path.sep + 'src' + path.sep + 'guards' + path.sep + 'jwt-auth.guard.ts');
await generateFile(
  path.sep + 'templates' + path.sep + 'guards' + path.sep + 'jwt-auth.guard.spec.ts',
  'jwt-auth.guard.spec.ts',
  path.sep + 'src' + path.sep + 'guard' + path.sep + 'jwt-auth.guard.spec.ts',
);
await generateFile(path.sep + 'templates' + path.sep + 'guards' + path.sep + 'roles.guard.ts', 'roles.guard.ts', path.sep + 'src' + path.sep + 'guards' + path.sep + 'roles.guard.ts');
await generateFile(path.sep + 'templates' + path.sep + 'guards' + path.sep + 'roles.guard.spec.ts', 'roles.guard.spec.ts', path.sep + 'src' + path.sep + 'guards' + path.sep + 'roles.guard.spec.ts');
await generateFile(path.sep + 'templates' + path.sep + 'guards' + path.sep + 'x-api-key.guard.ts', 'x-api-key.guard.ts', path.sep + 'src' + path.sep + 'guards' + path.sep + 'x-api-key.guard.ts');
await generateFile(
  path.sep + 'templates' + path.sep + 'guards' + path.sep + 'x-api-key.guard.spec.ts',
  'x-api-key.guard.spec.ts',
  path.sep + 'src' + path.sep + 'guards' + path.sep + 'x-api-key.guard.spec.ts',
);
