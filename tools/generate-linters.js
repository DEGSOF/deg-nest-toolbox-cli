import * as gtf from 'generate-template-files';
import * as path from 'path';
import * as url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const outputPath = process.env.INIT_CWD;

const generateFile = async (folderPath, slotValue, outputFileName) => {
  await gtf.generateTemplateFilesBatch([
    {
      option: '',
      entry: {
        folderPath: path.join(__dirname, folderPath),
      },
      defaultCase: gtf.CaseConverterEnum.KebabCase,
      dynamicReplacers: [
        {
          slot: '__entity__',
          slotValue,
        },
      ],
      output: {
        path: path.join(outputPath, outputFileName),
        overwrite: true,
      },
      onComplete: (result) => {
        console.log(`${outputFileName} file generated`);
      },
    },
  ]);
};

await generateFile('/templates/linters/.prettierrc', '.prettierrc', '/.prettierrc');
await generateFile('/templates/linters/.eslintrc.js', '.eslintrc.js', '/.eslintrc.js');
