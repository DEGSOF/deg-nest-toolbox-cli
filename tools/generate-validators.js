import * as gtf from 'generate-template-files';
import * as path from 'path';
import * as url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const outputPath = process.env.INIT_CWD;

const generateFile = async (folderPath, slotValue, outputFileName) => {
  await gtf.generateTemplateFilesBatch([
    {
      option: '',
      entry: {
        folderPath: path.join(__dirname, folderPath),
      },
      defaultCase: gtf.CaseConverterEnum.KebabCase,
      dynamicReplacers: [
        {
          slot: '__entity__',
          slotValue,
        },
      ],
      output: {
        path: path.join(outputPath, outputFileName),
        overwrite: true,
      },
      onComplete: (result) => {
        console.log(`${outputFileName} file generated`);
      },
    },
  ]);
};

// use the generateFile function to generate the files from the templates/guards folder
await generateFile('/templates/validators/deg-entityExists.spec.ts', 'deg-entityExists.spec.ts', '/src/validators/deg-entityExists.spec.ts');
await generateFile('/templates/validators/deg-entityExists.ts', 'deg-entityExists.ts', '/src/validators/deg-entityExists.ts');
await generateFile('/templates/validators/deg-entityExistsOrNull.spec.ts', 'deg-entityExistsOrNull.spec.ts', '/src/validators/deg-entityExistsOrNull.spec.ts');
await generateFile('/templates/validators/deg-entityExistsOrNull.ts', 'deg-entityExistsOrNull.ts', '/src/validators/deg-entityExistsOrNull.ts');
await generateFile('/templates/validators/deg-noEntityExists.spec.ts', 'deg-noEntityExists.spec.ts', '/src/validators/deg-noEntityExists.spec.ts');
await generateFile('/templates/validators/deg-noEntityExists.ts', 'deg-noEntityExists.ts', '/src/validators/deg-noEntityExists.ts');
