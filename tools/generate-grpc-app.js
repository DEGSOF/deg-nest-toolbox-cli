import * as gtf from 'generate-template-files';
import * as path from 'path';
import * as url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
let filePath = '';
let outputPath = '';
filePath = process.env.INIT_CWD + path.sep + 'src' + path.sep + 'app.module.ts';
outputPath = process.env.INIT_CWD + path.sep + 'src' + path.sep;

const generateTemplate = async (folderPath, slotValue, outputMessage) => {
  await gtf.generateTemplateFilesBatch([
    {
      entry: {
        folderPath: path.join(__dirname) + folderPath,
      },
      option: '',
      defaultCase: gtf.CaseConverterEnum.KebabCase,
      dynamicReplacers: [
        {
          slot: '__entity__',
          slotValue: slotValue,
        },
      ],
      output: {
        path: outputPath + path.sep + slotValue,
        overwrite: true,
      },
      onComplete: (result) => {
        console.log(outputMessage);
      },
    },
  ]);
};

await generateTemplate(path.sep + 'templates' + path.sep + 'grpc-app' + path.sep + 'app.module.ts', 'app.module.ts', 'App module generated');
await generateTemplate(path.sep + 'templates' + path.sep + 'grpc-app' + path.sep + 'modules', 'modules', 'Modules folder generated');
await generateTemplate(path.sep + 'templates' + path.sep + 'grpc-app' + path.sep + 'protos', 'protos', 'Protos folder generated');
await generateTemplate(path.sep + 'templates' + path.sep + 'grpc-app' + path.sep + 'main.ts', 'main.ts', 'Main file generated');
