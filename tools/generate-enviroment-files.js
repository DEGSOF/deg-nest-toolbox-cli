import * as gtf from 'generate-template-files';
import * as path from 'path';
import * as url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const outputPath = process.env.INIT_CWD;

const generateTemplate = async (folderPath, slotValue, outputMessage) => {
  await gtf.generateTemplateFilesBatch([
    {
      entry: {
        folderPath: path.join(__dirname, folderPath),
      },
      option: '',
      defaultCase: gtf.CaseConverterEnum.KebabCase,
      dynamicReplacers: [
        {
          slot: '__entity__',
          slotValue,
        },
      ],
      output: {
        path: outputPath + path.sep + slotValue,
        overwrite: true,
      },
      onComplete: (result) => {
        console.log(outputMessage);
      },
    },
  ]);
};

await generateTemplate(path.sep + 'templates' + path.sep + 'enviroment' + path.sep + 'dockerfile', 'dockerfile', 'Dockerfile generated');
await generateTemplate(path.sep + 'templates' + path.sep + 'enviroment' + path.sep + 'docker-compose.yml', 'docker-compose.yml', 'Docker compose file generated');
await generateTemplate(path.sep + 'templates' + path.sep + 'enviroment' + path.sep + '.env', '.env', '.env file generated');
await generateTemplate(path.sep + 'templates' + path.sep + 'enviroment' + path.sep + 'env.validation.ts', path.sep + 'env.validation.ts', 'env.validation.ts file generated');
