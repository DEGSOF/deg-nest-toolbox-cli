import * as fs from 'fs';
import * as gtf from 'generate-template-files';
import * as os from 'os';
import * as path from 'path';
import { normalize } from 'path';
import * as url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
let filePath = '';
let outputPath = '';
filePath = process.env.INIT_CWD + path.sep + 'src' + path.sep + 'app.module.ts';
outputPath = process.env.INIT_CWD + path.sep + 'src' + path.sep;

gtf.generateTemplateFiles([
  {
    option: 'GRPC Resource',
    entry: {
      folderPath: path.join(__dirname, 'templates', 'grpc-resource'),
    },
    defaultCase: gtf.CaseConverterEnum.KebabCase,
    stringReplacers: ['__entity__'],
    output: {
      path: path.join(outputPath, 'modules', '__entity__(kebabCase)'),
      overwrite: true,
    },
    onComplete: (result) => {
      result.output.files.forEach((file) => {
        if (file.match('.proto')) {
          file = file.replace(/\\/g, '\\');
          var protoName = file.split('\\').pop();
          if (!fs.existsSync(path.join(outputPath, 'protos'))) {
            fs.mkdirSync(path.join(outputPath, 'protos'), { recursive: false });
          }
          fs.rename(file, path.join(outputPath, 'protos', protoName), function (err) {
            if (err) {
              throw err;
            }
            fs.rmdir(file.replace(protoName, ''), function (err) {
              if (err) throw err;
            });
          });
        }
        if (file.match('module.ts')) {
          fs.readFile(file, { encoding: 'utf-8' }, function (err, data) {
            if (err) throw err;
            var array = data
              .trimEnd()
              .toString()
              .split(/\r\n|\r|\n/);
            var wordArray = array[array.length - 1].split(/[\s,]+/);
            var moduleName = wordArray[wordArray.length - 2];
            fs.readFile(filePath, { encoding: 'utf-8' }, function (err, data) {
              if (err) throw err;
              var array = data.toString().split(/\r\n|\r|\n/);
              let pathFile = `import { ${moduleName} } from ".${file.replace(normalize(outputPath), '\\').replace('.ts', '')}";`.replace('.\\', './').replace(/\\/g, '/');
              array.unshift(pathFile);
              for (const key in array) {
                if (array[key].includes('imports')) {
                  array.splice(Number(key) + Number(1), 0, moduleName + ',');
                }
              }
              fs.writeFile(filePath, array.join(os.EOL).replace(/\\/g, '/'), function (err) {
                if (err) throw err;
                console.log('Saved!');
              });
            });
          });
        }
      });
    },
  },
]);
