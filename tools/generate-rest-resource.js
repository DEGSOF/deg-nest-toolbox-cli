import * as fs from 'fs';
import * as gtf from 'generate-template-files';
import * as os from 'os';
import * as path from 'path';
import * as url from 'url';
import { normalize } from 'path';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
let filePath = '';
let outputPath = '';
if (fs.existsSync('apps')) {
  filePath = process.env.INIT_CWD + '/src/app/app.module.ts';
  outputPath = process.env.INIT_CWD + '/src/app/';
} else {
  filePath = process.env.INIT_CWD + '/src/app.module.ts';
  outputPath = process.env.INIT_CWD + '/src/';
}

gtf.generateTemplateFiles([
  {
    option: 'Resource',
    entry: {
      folderPath: path.join(__dirname) + '/templates/rest-resource',
    },
    defaultCase: gtf.CaseConverterEnum.KebabCase,
    stringReplacers: ['__entity__'],
    output: {
      path: outputPath + 'modules/__entity__(kebabCase)',
      overwrite: true,
    },
    onComplete: (result) => {
      result.output.files.forEach((file) => {
        if (file.match('module.ts')) {
          fs.readFile(file, { encoding: 'utf-8' }, function (err, data) {
            if (err) throw err;
            var array = data
              .trimEnd()
              .toString()
              .split(/\r\n|\r|\n/);
            var wordArray = array[array.length - 1].split(/[\s,]+/);
            var moduleName = wordArray[wordArray.length - 2];
            fs.readFile(filePath, { encoding: 'utf-8' }, function (err, data) {
              if (err) throw err;
              var array = data.toString().split(/\r\n|\r|\n/);
              let pathFile = `import { ${moduleName} } from ".${file.replace(normalize(outputPath), '/').replace('.ts', '')}";`.replace('./', './').replace('/', '/');
              array.unshift(pathFile);
              for (const key in array) {
                if (array[key].includes('imports')) {
                  array.splice(Number(key) + Number(1), 0, moduleName + ',');
                }
              }
              fs.writeFile(filePath, array.join(os.EOL), function (err) {
                if (err) throw err;
                console.log('Saved!');
              });
            });
          });
        }
      });
    },
  },
]);
