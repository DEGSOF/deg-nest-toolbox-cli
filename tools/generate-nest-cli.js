import * as gtf from 'generate-template-files';
import * as path from 'path';
import * as url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const outputPath = process.env.INIT_CWD;

const generateTemplate = async (folderPath, slotValue, outputMessage) => {
  await gtf.generateTemplateFilesBatch([
    {
      entry: {
        folderPath: path.join(__dirname, folderPath),
      },
      option: '',
      defaultCase: gtf.CaseConverterEnum.KebabCase,
      dynamicReplacers: [
        {
          slot: '__entity__',
          slotValue,
        },
      ],
      output: {
        path: outputPath + '/' + slotValue,
        overwrite: true,
      },
      onComplete: (result) => {
        console.log(outputMessage);
      },
    },
  ]);
};

await generateTemplate('/templates/nest-cli.json', 'nest-cli.json', 'nest-cli.json generated');
