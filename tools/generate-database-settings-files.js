import * as gtf from 'generate-template-files';
import * as path from 'path';
import * as url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const outputPath = process.env.INIT_CWD + path.sep + 'src' + path.sep;

await gtf.generateTemplateFilesBatch([
  {
    entry: {
      folderPath: path.join(__dirname, 'templates', 'database'),
    },
    option: '',
    defaultCase: gtf.CaseConverterEnum.KebabCase,
    dynamicReplacers: [
      {
        slot: '__entity__',
        slotValue: 'database',
      },
    ],
    output: {
      path: path.join(outputPath, 'database'),
      overwrite: true,
    },
    onComplete: (result) => {
      console.log('Database folder generated');
    },
  },
]);
