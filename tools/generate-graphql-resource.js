import * as fs from 'fs';
import * as gtf from 'generate-template-files';
import * as os from 'os';
import * as path from 'path';
import * as url from 'url';
import { normalize } from 'path';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
let filePath = '';
let outputPath = '';
filePath = path.resolve(process.env.INIT_CWD, 'src', 'app.module.ts');
outputPath = path.resolve(process.env.INIT_CWD, 'src');

gtf.generateTemplateFiles([
  {
    option: 'Resource',
    entry: {
      folderPath: path.join(__dirname, 'templates', 'graphql-resource'),
    },
    defaultCase: gtf.CaseConverterEnum.KebabCase,
    stringReplacers: ['__entity__'],
    output: {
      path: path.join(outputPath, 'modules', '__entity__(kebabCase)'),
      overwrite: true,
    },
    onComplete: (result) => {
      result.output.files.forEach((file) => {
        if (file.match('module.ts')) {
          fs.readFile(file, { encoding: 'utf-8' }, function (err, data) {
            if (err) {
              console.error(err);
              return;
            }
            const lines = data
              .trimEnd()
              .toString()
              .split(/\r\n|\r|\n/);
            const words = lines[lines.length - 1].split(/[\s,]+/);
            const moduleName = words[words.length - 2];
            fs.readFile(filePath, { encoding: 'utf-8' }, function (err, data) {
              if (err) {
                console.error(err);
                return;
              }
              const fileLines = data.toString().split(/\r\n|\r|\n/);
              const importStatement = `import { ${moduleName} } from ".${file.replace(normalize(outputPath), path.sep).replace('.ts', '')}";`.replace('.' + path.sep, './').replace(path.sep, '/');
              fileLines.unshift(importStatement);
              for (const key in fileLines) {
                if (fileLines[key].includes('imports')) {
                  fileLines.splice(Number(key) + Number(1), 0, moduleName + ',');
                }
              }
              const lineEnding = data.includes('\r\n') ? '\r\n' : data.includes('\r') ? '\r' : '\n';
              fs.writeFile(filePath, fileLines.join(lineEnding), function (err) {
                if (err) {
                  console.error(err);
                  return;
                }
                console.log('Saved!');
              });
            });
          });
        }
      });
    },
  },
]);
